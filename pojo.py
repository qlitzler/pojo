import os
import sys
import json
import pprint
import shutil
from urllib.request import urlopen
import argparse

from collections import OrderedDict

def main():
	args = arguments()
	try:
		url = urlopen(args.url)
	except Exception as e:
		print(e)
	else:
		if os.path.exists('build'):
			shutil.rmtree('build')
		os.mkdir('build')
		with open("pojo.config", "r") as f:
			global config
			config = json.loads(f.read())
			parser(json.loads(url.read().decode('utf-8')), config["main"])


def arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument('url')
	return parser.parse_args()

def parser(json, name):
	pojo = []
	if not isinstance(json, str):
		for key in json:
			leaf = json[key]
			pojo.append((key, type(leaf).__name__))
			if isinstance(leaf, dict):
				parser(leaf, key)
			elif isinstance(leaf, list):
				if len(leaf) > 0:
					parser(leaf[0], key)
		builder(pojo, name if name[0].isupper() else name.capitalize())

def builder(pojo, name):
	with open(FILE % name, "w+") as f:
		f.write(config["package"])
		f.write(config["gson"])
		f.write(config["class"] % name)
	# v = Value, t = Type
		tuples = [formater(v, t) for v, t in pojo]
		f.writelines([config["const"] % (v.upper(), v) for v, t in tuples])
		f.writelines([config["attr"] % (v.upper(), t, v) for v, t in tuples])
		f.writelines([config["get"] % (t, v.capitalize(), v) for v, t in tuples])
	#	f.writelines([config["set"] % (v.capitalize(), t, v, v, v) for v, t in tuples])
		f.write(config["end"])

def formater(v, t):
	if v[0].isdigit():
		v = 'n' + v
	if t == 'list':
		return v, config["list"] % v.capitalize()
	elif t == 'str':
		return v, config["string"]
	elif t == 'dict':
		return v, v.capitalize()
	elif t == 'bool':
		return v, config["boolean"]
	return v, t

FILE = "build/%s.java"


if __name__ == "__main__":
    main()
